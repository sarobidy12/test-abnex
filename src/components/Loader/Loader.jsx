import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = (props) => {
    return props.loading ? (<center><br/><CircularProgress disableShrink /> </center>) : props.component
}

export default Loader
