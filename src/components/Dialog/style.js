import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
    },
    sendIcon: {
        cursor: 'pointer'
    },
    textField: {
        padding: '1vh',
        backgroundColor: theme.palette.background.paper,
    },
    dialogContent: {
        backgroundColor: theme.palette.background.paper,
        margin: 0
    }
}));

export default useStyles;