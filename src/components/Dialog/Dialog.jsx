import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import SendIcon from '@material-ui/icons/Send';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import useStyles from './style';
import config from "../../config";
import Loader from '../Loader';
import axios from 'axios';


const DialogComment = (props) => {

    var { open, setOpen, vehicleSelected } = props;
    const classes = useStyles();
    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([])
    const [comment, setComment] = useState("")

    const fetchData = async () => {
        var response = await axios.get(`${config.servers.apiUrl}comments?postId=${vehicleSelected.id}`)

        if (response) {
            setLoading(false);
            setList(response.data);
        }
    }

    useEffect(() => {
        if (list.length === 0 && open) {
            fetchData();
        }
    });

    const handleClose = () => {
        setOpen(false);
        setList([])
        setLoading(true);
    };

    const sendComment = (e) => {
        e.preventDefault();

        var user = JSON.parse(sessionStorage.getItem("user"));

        var dataSend = {
            "postId": vehicleSelected.id,
            "name": user.name,
            "email": user.users,
            "body": comment
        }

        axios.post(`${config.servers.apiUrl}comments`, {
            ...dataSend
        }).then((res) => {

            var Newlist = [...list]
            Newlist.push(res.data);
            setList([...Newlist]);
        })

    }

    const handleChange = (e) => {
        setComment(e.target.value);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                scroll='paper'
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >

                <DialogTitle className={classes.root} id="scroll-dialog-title">{vehicleSelected.title}</DialogTitle>
                <DialogContent dividers={true} className={classes.dialogContent} >
                    <Loader
                        loading={loading}
                        component={list.map((value) => {
                            return (<ListItem button>
                                <ListItemText primary={value.name} secondary={value.body} />
                            </ListItem>)
                        },
                        )
                        } />
                </DialogContent>
                <div className={classes.textField} >
                    <form onSubmit={sendComment}>
                        <TextField
                            margin="normal"
                            required
                            multiline
                            fullWidth
                            onChange={handleChange}
                            id="email"
                            label="Votre commentaire"
                            name="comment"
                            InputProps={{
                                endAdornment: <SendIcon onClick={sendComment} color="primary" className={classes.sendIcon} />,
                            }}
                        />
                    </form>
                </div>
                <DialogActions className={classes.root} >
                    <Button onClick={handleClose} color="primary">
                        FERMER
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
export default DialogComment;
