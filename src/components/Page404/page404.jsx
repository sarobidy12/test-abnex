import Container from '@material-ui/core/Container';

const page404 = () => {

    return (
        <Container component="main" maxWidth="xs">
            <h1>
                Page introuvable
            </h1>
        </Container>
    );
}

export default page404;