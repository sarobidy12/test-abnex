import { useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useHistory } from 'react-router';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import axios from 'axios';
import useStyles from './style';
import config from "../../config";
import Loader from '../Loader';


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function SignIn() {
  const classes = useStyles();
  const history = useHistory();
  const [mail, setMail] = useState("");
  const [isNotFound, setIsNotFound] = useState(false);
  const [loading, setLoading] = useState(false);

  const login = (e) => {

    e.preventDefault();

    const fetchData = async () => {

      setLoading(true)
      var response = await axios.get(`${config.servers.apiUrl}users?email=${mail}`)

      if (response.data.length > 0) {
        sessionStorage.setItem("user", JSON.stringify(response.data[0]))
        history.push('/');
      }

      if (response.data.length === 0) {
        setIsNotFound(true);
      }

      setLoading(false)
    }

    fetchData();

  }

  const handleChange = (e) => {
    setMail(e.target.value);
  }

  const handleClose = (event, reason) => {
    setIsNotFound(false);
  };

  const listTest = ['Sincere@april.biz', 'hildegard.org', 'Shanna@melissa.tv', 'anastasia.net']

  useEffect(() => {
    var rand = Math.floor(Math.random() * (3 - 0 + 1) + 0);
    setMail(listTest[rand])
  }, [setMail])
  return (

    <Container component="main" maxWidth="xs">
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={isNotFound}
        onClose={handleClose}
      >

        <Alert severity="error">Address e-mail introuvable </Alert>

      </Snackbar>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          CONNEXION
        </Typography>
        <form className={classes.form} onSubmit={(e) => login(e)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            value={mail || ""}
            onChange={handleChange}
            autoComplete="email"
            autoFocus
          />
          <Loader
            loading={loading}
            component={
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                CONNEXION
              </Button>
            } />
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}