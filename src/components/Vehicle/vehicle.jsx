import React, { useEffect, useState } from 'react';
import useStyles from './style';
import List from '@material-ui/core/List';
import Badge from '@material-ui/core/Badge';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import CommentIcon from '@material-ui/icons/Comment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import CloseIcon from '@material-ui/icons/Close';
import Header from '../Header';
import Loader from '../Loader';
import Dialog from '../Dialog';
import config from "../../config";
import axios from 'axios';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Vehicle = () => {

    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);
    const [listCommentaire, setListCommentaire] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);
    const [vehicleSelected, setVehicleSelected] = useState({});
    const [open, setOpen] = useState(false);

    const fetchData = async () => {
        var response1 = await axios.get(`${config.servers.apiUrl}posts`)
        var response2 = await axios.get(`${config.servers.apiUrl}comments`)

        if (response1 && response2) {
            setLoading(false);
            setList(response1.data);
            setListCommentaire(response2.data);
        }
    }

    var user = JSON.parse(sessionStorage.getItem("user"));

    useEffect(() => {

        fetchData();

    }, [setLoading, setList]);

    const classes = useStyles();

    const comment = (comment) => () => {
        if (user) {
            setVehicleSelected({
                ...comment
            });

            setOpenDialog(true);
            return;
        }
        setOpen(true)
    }

    const handleChange = (e) => {

        if (e.target.value !== "") {
            setList([...list].filter(items => items.title.includes(e.target.value)));
            return;
        }

        fetchData()

    }


    const handleClose = (event, reason) => {
        setOpen(false);
    };

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                open={open}
                onClose={handleClose}
            >

                <Alert severity="warning">Connecter-vous pour voire et faire des commentaire
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleClose}>
                        <CloseIcon />
                    </IconButton>
                </Alert>



            </Snackbar>
            <Header onChange={handleChange} />
            <Dialog
                open={openDialog}
                setOpen={setOpenDialog}
                vehicleSelected={vehicleSelected}
            />
            <Loader
                loading={loading}
                component={
                    <List className={classes.root}>
                        {list.map((value) => {
                            const labelId = `checkbox-list-label-${value.id}`;

                            return (
                                <ListItem className={classes.listItem} key={value} role={undefined} dense button
                                >
                                    <ListItemText id={labelId} primary={`${value.title}`} />
                                    <ListItemSecondaryAction>
                                        <IconButton edge="end" aria-label="comments">
                                            <Badge badgeContent={listCommentaire.filter(items => items.postId === value.id).length} color="secondary">
                                                <CommentIcon onClick={comment(value)} />
                                            </Badge>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        })}
                    </List>
                }
            />

        </div>

    );
}

export default Vehicle;