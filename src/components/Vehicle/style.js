import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '80%',
        backgroundColor: theme.palette.background.paper,
        margin: '5vh auto'
    },
    listItem:{
        padding:'2vh'
    }
}));

export default useStyles;