import DateFnsUtils from '@date-io/date-fns';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { BrowserRouter } from 'react-router-dom';
import Router from './Router';
import myTheme from './Theme';
import fr from 'date-fns/locale/fr';


function App() {

  // Component will mount check for the session check

  return (

    <MuiThemeProvider theme={myTheme}>

      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={fr}>

        <BrowserRouter>

          <div className="app">
            <Router />
          </div>

        </BrowserRouter>

      </MuiPickersUtilsProvider>

    </MuiThemeProvider>

  );
}

export default App;
