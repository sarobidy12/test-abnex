import { Route, Switch } from 'react-router-dom';
import Page404 from '../components/Page404';
import login from '../components/Login';
import vehicle from '../components/Vehicle';

const Router = () => {

    return (
        <Switch>

            <Route path="/login" exact={true} component={login} />
            <Route path="/" exact={true} component={vehicle} />

            <Route path="*">
                <Page404 />
            </Route>

        </Switch>
    );
};

export default Router;
